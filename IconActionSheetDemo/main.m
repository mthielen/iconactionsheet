//
//  main.m
//  IconActionSheetDemo
//
//  Created by Markus Thielen on 6.10.2012.
//  Copyright (c) 2012 thi.guten. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "IASDAppDelegate.h"

int main(int argc, char *argv[])
{
  @autoreleasepool {
      return UIApplicationMain(argc, argv, nil, NSStringFromClass([IASDAppDelegate class]));
  }
}
