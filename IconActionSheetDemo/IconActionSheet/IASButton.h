/*******************************************************************************
 *
 * IASButton.h - class for a single Icon Action Sheet button
 *
 * Created by Markus Thielen on 6.10.2012.
 * Copyright (c) 2012 thi.guten. All rights reserved.
 */

/*---- imports ---------------------------------------------------------------*/

#import <UIKit/UIKit.h>


/*---- defines ---------------------------------------------------------------*/

#define MAX_IMG_CX    60
#define MAX_IMG_CY    MAX_IMG_CX


/*---- protocol definitions --------------------------------------------------*/

@protocol IASButtonDelegate <NSObject>

- (void) buttonWasTouched;

@end

/*---- interface -------------------------------------------------------------*/

@interface IASButton : UIButton
{
  UIImage *_image;
  UIImage *_hilitImage;
  BOOL _isHighlighted;
  void (^_block)(void);
  id <IASButtonDelegate> _delegate;
}

/*---- instance methods ------------------------------------------------------*/

- (id) initWithImage:(UIImage*)image
                text:(NSString*)text
               block:(void (^)())block;


/*---- properties ------------------------------------------------------------*/

@property (nonatomic, retain) id <IASButtonDelegate> delegate;

@end
