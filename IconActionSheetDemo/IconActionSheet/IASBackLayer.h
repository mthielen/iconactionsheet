/*******************************************************************************
 *
 * IASBackLayer.h
 *
 * semi-transparent background layer for the icon action sheet
 *
 * Created by Markus Thielen on 7.10.2012.
 * Copyright (c) 2012 thi.guten. All rights reserved.
 *
 */

/*---- imports ---------------------------------------------------------------*/

#import <QuartzCore/QuartzCore.h>



@interface IASBackLayer : CAGradientLayer

- (id) initWithBounds:(CGRect)bounds;

@end
