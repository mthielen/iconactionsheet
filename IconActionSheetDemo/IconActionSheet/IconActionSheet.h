/*******************************************************************************
 *
 * IconActionSheet.m
 *
 * Created by Markus Thielen on 6.10.2012.
 * Copyright (c) 2012 thi.guten. All rights reserved.
 */

/*---- imports ---------------------------------------------------------------*/

#import <UIKit/UIKit.h>
#import "IASButton.h"

@interface IconActionSheet : UIView <IASButtonDelegate>
{
  NSMutableArray *_buttons;
  UIView *_parent;
  UIView *_coverView;
}

- (void) addButtonWithImage:(UIImage*)image
                       text:(NSString*)text
                      block:(void (^)())block;

- (void) showWithDismissButtonTitle:(NSString*)dismissText;

@end
