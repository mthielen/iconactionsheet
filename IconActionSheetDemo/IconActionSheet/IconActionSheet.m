/*******************************************************************************
 *
 * IconActionSheet.m
 *
 * Created by Markus Thielen on 6.10.2012.
 * Copyright (c) 2012 thi.guten. All rights reserved.
 */

/*---- imports ---------------------------------------------------------------*/

#import "IconActionSheet.h"
#import <QuartzCore/QuartzCore.h>
#import "IASBackLayer.h"

/*---- defines ---------------------------------------------------------------*/

#define MAX_BUTTONS       9   /* 3 rows with 3 buttons each */
#define MAX_BTNS_PER_ROW  3

#define TOP_MARGIN            35
#define LEFT_MARGIN           30

#define ROW_HEIGHT            101
#define COL_WIDTH             100

#define CANCEL_BTN_WIDTH      280
#define CANCEL_BTN_HEIGHT     46
#define CANCEL_BTN_TOP_GAP    10
#define CANCEL_BTN_BOTTOM_GAP 20


/*---- data ------------------------------------------------------------------*/

static NSString * const logPrefix = @"IconActionSheet: ";

@implementation IconActionSheet

/*******************************************************************************
 * init
 */
- (id)init
{
  self = [super init];
  if (self) {
    _buttons = [NSMutableArray new];
    self.backgroundColor = [UIColor clearColor];
  }
  
  return self;
}

/*******************************************************************************
 * dealloc
 */
- (void) dealloc
{
  [_buttons release];
  [super dealloc];
}

/*******************************************************************************
 * add button with image and text
 */
- (void) addButtonWithImage:(UIImage *)image text:(NSString *)text
                      block:(void (^)())block;
{
  NSAssert1([_buttons count] < MAX_BUTTONS, @"Can't add more than %d buttons",
            MAX_BUTTONS);

  /* add button to our buttons array */
  IASButton *btn = [[[IASButton alloc] initWithImage:image text:text block:block] autorelease];
  btn.delegate = self;
  [_buttons addObject:btn];
}

/*******************************************************************************
 * show - calculates the height of the view, adds the buttons and
 * animates the action sheet into view.
 */
- (void) showWithDismissButtonTitle:(NSString*)dismissText
{
  int btnRows = ([_buttons count] + 2) / MAX_BTNS_PER_ROW;
  int btnIdx = 0;
  _parent = [UIApplication sharedApplication].keyWindow.rootViewController.view;
  
  /* calculate size and position so the sheet is positioned right outside
   * its parent */
  CGFloat cy = TOP_MARGIN + (ROW_HEIGHT * btnRows) +
               CANCEL_BTN_TOP_GAP + CANCEL_BTN_HEIGHT + CANCEL_BTN_BOTTOM_GAP;
  CGFloat cx = _parent.bounds.size.width;
  self.frame = CGRectMake(0, _parent.bounds.size.height, cx, cy);
  
  /* create the background layer */
  IASBackLayer *backLayer = [[IASBackLayer alloc] initWithBounds:self.bounds];
  [self.layer addSublayer:backLayer];
  [backLayer release];
  
  
  CGFloat y = TOP_MARGIN;

  for (int row = 0; row < btnRows; row++) {
    CGFloat x = LEFT_MARGIN;
    /* add buttons for this row */
    for (int col = 0; col < MAX_BTNS_PER_ROW && btnIdx < [_buttons count]; col++) {
      IASButton *btn = (IASButton*)[_buttons objectAtIndex:btnIdx++];
      btn.frame = CGRectMake(x, y, btn.bounds.size.width, btn.bounds.size.height);
      [self addSubview:btn];
      x += COL_WIDTH;
    }
    
    y += ROW_HEIGHT;

  }
  
  /* create dismiss button */
  y += CANCEL_BTN_TOP_GAP;
  UIButton *dismissBtn = [UIButton buttonWithType:UIButtonTypeCustom];
  dismissBtn.frame = CGRectMake((cx - CANCEL_BTN_WIDTH) / 2, y,
                                CANCEL_BTN_WIDTH, CANCEL_BTN_HEIGHT);
  /* set button background image */
  UIImage *img;
  img = [UIImage imageNamed:@"action-black-button.png"];
  img = [img stretchableImageWithLeftCapWidth:10 topCapHeight:0];
  [dismissBtn setBackgroundImage:img forState:UIControlStateNormal];
  img = [UIImage imageNamed:@"action-black-button-selected.png"];
  img = [img stretchableImageWithLeftCapWidth:10 topCapHeight:0];
  [dismissBtn setBackgroundImage:img forState:UIControlStateSelected];
  
  dismissBtn.titleLabel.font = [UIFont boldSystemFontOfSize:18];
  dismissBtn.titleLabel.textColor = [UIColor whiteColor];
  
  [dismissBtn setTitle:dismissText forState:UIControlStateNormal];
  [dismissBtn addTarget:self action:@selector(dismiss) forControlEvents:UIControlEventTouchUpInside];
  [self addSubview:dismissBtn];
  
  /* create view that covers the parent with opacity */
  _coverView = [[UIView alloc] initWithFrame:_parent.bounds];
  _coverView.alpha = 0;
  _coverView.backgroundColor = [UIColor blackColor];
  [_parent addSubview:_coverView];
  [_coverView release];

  /* set top shadow */
  self.layer.shadowOpacity = 1;
  self.layer.shadowColor = [UIColor blackColor].CGColor;
  self.layer.shadowOffset = CGSizeMake(0, -1);
  self.layer.shadowRadius = .7;
  
  /* add ourselves to the main view of the app */
  [_parent addSubview:self];
  
  /* show the action sheet */
  [UIView animateWithDuration:.5 animations:^{
    self.frame = CGRectMake(0, _parent.bounds.size.height - cy, cx, cy);
    _coverView.alpha = .5;
  }];
  
}

/*******************************************************************************
 * remove ourselves from the screen
 */
- (void) dismiss
{
  [UIView animateWithDuration:.5 animations:^{
    /* fade out the cover view */
    _coverView.alpha = 0;
    /* move down and out of view */
    self.frame = CGRectMake(0, _parent.bounds.size.height,
                            self.frame.size.width, self.frame.size.height);
  } completion:^(BOOL finished) {
    [self removeFromSuperview];
    [_coverView removeFromSuperview];
  }];
  
}

/*******************************************************************************
 * delegate method: some button was touched
 */
- (void) buttonWasTouched
{
  [self dismiss];
}

/*******************************************************************************
 * drawRect - draw the bright reflection line at the top. We cannot do this
 * in the background layer since it's semi transparent, which results in a dull
 * line.
 */
- (void) drawRect:(CGRect)rect
{
  CGContextRef ctx = UIGraphicsGetCurrentContext();
  CGContextSaveGState(ctx);
  CGContextSetAlpha(ctx, 1);
  CGContextSetAllowsAntialiasing(ctx, NO);

  CGContextBeginPath(ctx);
  CGContextSetRGBStrokeColor(ctx, .65, .65, .65, 1);
  CGContextMoveToPoint(ctx, .5, .5);
  CGContextAddLineToPoint(ctx, self.bounds.size.width - .5, .5);
  CGContextStrokePath(ctx);

  CGContextRestoreGState(ctx);
}

@end
