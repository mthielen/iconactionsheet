/*******************************************************************************
 *
 * IASBackLayer.m
 *
 * semi-transparent background layer for the icon action sheet
 *
 * Created by Markus Thielen on 7.10.2012.
 * Copyright (c) 2012 thi.guten. All rights reserved.
 *
 */

/*---- imports ---------------------------------------------------------------*/

#import "IASBackLayer.h"


/*---- defines ---------------------------------------------------------------*/

#define GRADIENT_HEIGHT       40.0
#define GRADIENT_OUTER_COLOR  ([UIColor colorWithWhite:.6 alpha:0.3].CGColor)

@implementation IASBackLayer

/*******************************************************************************
 * init
 */
- (id) initWithBounds:(CGRect)bounds
{
  self = [super init];
  if (self) {
    self.bounds = bounds;
    self.anchorPoint = CGPointMake(0, 0);
    /* define gradient */
    self.colors = [NSArray arrayWithObjects:(id)GRADIENT_OUTER_COLOR,
                                            [UIColor blackColor].CGColor,
                                            nil];
    self.startPoint = CGPointMake(0, 0);
    self.endPoint = CGPointMake(0, 1.0f / (bounds.size.height / GRADIENT_HEIGHT));
    self.opacity = .5;
  }
  return self;
}

@end
