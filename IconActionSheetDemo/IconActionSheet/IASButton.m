/*******************************************************************************
 *
 * IASButton.h - class for a single Icon Action Sheet button
 *
 * Created by Markus Thielen on 6.10.2012.
 * Copyright (c) 2012 thi.guten. All rights reserved.
 */

/*---- imports ---------------------------------------------------------------*/

#import "IASButton.h"


/*---- defines ---------------------------------------------------------------*/

#define BTN_LABEL_FONT          [UIFont boldSystemFontOfSize:11.0]
#define BTN_LABEL_COLOR         [UIColor whiteColor]
#define BTN_LABEL_SHADOW_COLOR  [UIColor blackColor]
#define BTN_INNER_GAP           0

#define BTN_WIDTH               75
#define BTN_HEIGHT              95
#define DEFAULT_IMAGE_HEIGHT    60


@implementation IASButton

/*******************************************************************************
 * init
 */
- (id) initWithImage:(UIImage *)image text:(NSString *)text block:(void (^)())block
{
  self = [super initWithFrame:CGRectMake(0, 0, BTN_WIDTH, BTN_HEIGHT)];
  if (self) {

    /* verify arguments */
    NSAssert(image && block && text, @"nil argument passed to IASButton init");
    NSAssert2(image.size.width <= MAX_IMG_CX && image.size.height <= MAX_IMG_CY,
              @"image passed to IASButton init is larger that %dx%d",
              MAX_IMG_CX, MAX_IMG_CY);

    _image = [image retain];
    _block = [block copy];
    
    self.opaque = NO;
    
    /* create label */
    UILabel *label;
    label = [[UILabel alloc] init];
    label.font = BTN_LABEL_FONT;
    label.textColor = BTN_LABEL_COLOR;
    label.shadowColor = BTN_LABEL_SHADOW_COLOR;
    label.shadowOffset = CGSizeMake(1, 1);
    label.textAlignment = UITextAlignmentCenter;
    label.text = text;
    label.backgroundColor = [UIColor clearColor];
    label.numberOfLines = 2;
    label.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    label.frame = CGRectMake(0, MAX_IMG_CY + BTN_INNER_GAP,
                             BTN_WIDTH, BTN_HEIGHT - MAX_IMG_CY - BTN_INNER_GAP);
    [self addSubview:label];
    [label release];
    
    /* create highlighted image */    
    UIGraphicsBeginImageContext(_image.size);
    CGRect drawRect = CGRectMake(0, 0, _image.size.width, _image.size.height);
    [_image drawInRect:drawRect];
    [[UIColor colorWithWhite:0 alpha:.5] set];
    UIRectFillUsingBlendMode(drawRect, kCGBlendModeSourceAtop);
    _hilitImage = UIGraphicsGetImageFromCurrentImageContext();
    [_hilitImage retain];
    UIGraphicsEndImageContext();

    
    /* handle touch events */
    [self addTarget:self action:@selector(touchDown) forControlEvents:UIControlEventTouchDown];
    [self addTarget:self action:@selector(trigger) forControlEvents:UIControlEventTouchUpInside];
    [self addTarget:self action:@selector(touchUp) forControlEvents:UIControlEventTouchUpOutside];
    
  }
  
  return self;
}

/*******************************************************************************
 * dealloc
 */
- (void) dealloc
{
  [_image release];
  [_hilitImage release];
  [_block release];
  self.delegate = nil;
  [super dealloc];
}

/*******************************************************************************
 * drawRect
 */
- (void)drawRect:(CGRect)rect
{
  UIImage *img = _isHighlighted ? _hilitImage : _image;
  [img drawAtPoint:CGPointMake((BTN_WIDTH - _image.size.width) / 2,
                               MAX(0, (DEFAULT_IMAGE_HEIGHT - _image.size.height) / 2))];
}

/*******************************************************************************
 * touch down event - draw visual feed back for the touch
 */
- (void) touchDown
{
  _isHighlighted = YES;
  [self setNeedsDisplay];
}

/*******************************************************************************
 * touch up event - clear hilight state
 */
- (void) touchUp
{
  _isHighlighted = NO;
  [self setNeedsDisplay];
}

/*******************************************************************************
 * trigger - button was triggered
 */
- (void) trigger
{
  [self touchUp];
  [_delegate buttonWasTouched];
  _block();
}

@end
