//
//  IASDViewController.m
//  IconActionSheetDemo
//
//  Created by Markus Thielen on 6.10.2012.
//  Copyright (c) 2012 thi.guten. All rights reserved.
//

#import "IASDViewController.h"
#import "IconActionSheet.h"

@interface IASDViewController ()

@end

@implementation IASDViewController

/*******************************************************************************
 * view did load
 */
- (void)viewDidLoad
{
  [super viewDidLoad];
  
  /* load some web content for demo purposes */
  UIWebView *wv = (UIWebView*)[self.view viewWithTag:111];
  [wv loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:@"http://www.wikipedia.org"]]];
}

/*******************************************************************************
 * memory warning received
 */
- (void)didReceiveMemoryWarning
{
  [super didReceiveMemoryWarning];
  // Dispose of any resources that can be recreated.
}

/*******************************************************************************
 * show the icon action sheet
 */
- (IBAction)showIconActionSheet
{
  IconActionSheet *ias = [[IconActionSheet alloc] init];
  
  UIImage *icon = [UIImage imageNamed:@"Icon.png"];
  
  [ias addButtonWithImage:icon
                     text:@"Do Some-Thing"
                    block:^{
                      NSLog(@"Something pressed");
                    }];
  [ias addButtonWithImage:icon
                     text:@"Do Hoola-Hoop"
                    block:^{
                      NSLog(@"Hoola Hoop");
                    }];
  
  [ias addButtonWithImage:icon
                     text:@"Call Mom"
                    block:^{
                      NSLog(@"Call Mom pressed");
                    }];
  
  [ias addButtonWithImage:icon
                     text:@"Mail Dad"
                    block:^{
                      NSLog(@"Mail Dad pressed");
                    }];
  
  [ias addButtonWithImage:icon
                     text:@"Do No-Thing"
                    block:^{
                      NSLog(@"Do nothing pressed");
                    }];
  
  
  [ias showWithDismissButtonTitle:@"Cancel"];
  [ias release];
  
}
@end
