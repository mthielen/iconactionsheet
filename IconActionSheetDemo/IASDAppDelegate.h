/*******************************************************************************
 * 
 *  IASDAppDelegate.h
 *  IconActionSheetDemo
 *
 *  Created by Markus Thielen on 6.10.2012.
 *  Copyright (c) 2012 thi.guten. All rights reserved.
 */

/*---- imports ---------------------------------------------------------------*/

#import <UIKit/UIKit.h>


@class IASDViewController;

@interface IASDAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) IASDViewController *viewController;

@end
