//
//  IASDViewController.h
//  IconActionSheetDemo
//
//  Created by Markus Thielen on 6.10.2012.
//  Copyright (c) 2012 thi.guten. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IASDViewController : UIViewController

- (IBAction)showIconActionSheet;

@end
